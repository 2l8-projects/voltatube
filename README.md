![logo](assets/images/logo/logo.png?raw=true)

# VoltaTube

VoltaTube is streaming software similar to [YouTube](https://youtube.com) or [Netflix](https://netflix.com).

## Features
* Uses PDO for MySQL Connection
* User upload
  * Private
  * Public
  * Category choice
  * Description
* Twitter/Bootstrap
* FontAwesome

## Reqiures
* Webserver
  * Apache2
  * nGinX
* PHP 7+
  * Developed on PHP7.3


## License


## Documentation
Some documentation might be found here on the [wiki](../../wikis/home/). Most will be added on [the project's website](https://voltatube.eu), which will also have a [forum](https://voltatube.eu/forum).
