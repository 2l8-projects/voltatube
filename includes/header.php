<?php require_once("includes/config.php"); ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<!--
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
-->
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="assets/css/master.css">

    <link rel="icon" type="image/png" href="assets/images/logo/favicon.png"/>
    <title>VoltaTube</title>
  </head>
  <body>
    <!--<img src="assets/images/logo/logo.png" alt="VoltaTube Logo" height="30px">-->

    <div id="pageContainer"><!-- container -->

      <div id="mastHeadContainer"><!-- navbar -->
        <button class="navShowHide">
          <img src="assets/images/icons/menu.png" alt="menu">
        </button>

        <a class="logoContainer" href="index.php">
          <img src="assets/images/logo/logo.png" alt="Site Logo" title="VoltaTube Logo" />
        </a>

        <div class="searchBarContainer">
          <form class="" action="search.php" method="GET">
            <input type="text" name="term" value="" class="searchBar" placeholder="Search...">
              <button class="searchButton">
                <img src="assets/images/icons/search.png" alt="search">
              </button>
          </form>
        </div>

        <div class="rightIcons">
          <a href="upload.php">
            <img class="upload" src="assets/images/icons/upload.png" alt="upload">
          </a>
          <a href="#">
            <img src="assets/images/profile/default.png" alt="profile">
          </a>
        </div>

      </div><!-- end navbar -->

      <div id="sideNavContainer" style="display: none"><!-- sidebar -->

      </div><!-- end sidebar -->

      <div id="mainSectionContainer"><!-- main section -->

        <div id="mainContentContainer"><!-- main content -->
