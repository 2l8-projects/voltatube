<?php
ob_start(); //Turns on output buffering

date_default_timezone_set("Europe/London");

try {
    $con = new PDO("mysql:dbname=c0_voltatube_hpk_db1;host=localhost", "c0_hpk", "feder04991");
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
}
catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
?>
